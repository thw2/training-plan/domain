package de.danisbu.thw.trainingplan.modell

public data class TrainingList (val list: MutableList<Training> = mutableListOf())