package de.danisbu.thw.trainingplan.modell

import kotlinx.datetime.LocalDateTime

data class End (val value: LocalDateTime)
