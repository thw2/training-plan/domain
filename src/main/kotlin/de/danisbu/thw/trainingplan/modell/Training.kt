package de.danisbu.thw.trainingplan.modell

data class Training(
    val id: TrainingId,
    val start: Start,
    val end: End,
    val registrationDeadline: RegistrationDeadline?,
    val course: Course,
    val location: Location
)