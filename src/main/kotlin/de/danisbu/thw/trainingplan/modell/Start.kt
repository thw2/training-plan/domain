package de.danisbu.thw.trainingplan.modell

import kotlinx.datetime.LocalDateTime

data class Start (val value: LocalDateTime)
