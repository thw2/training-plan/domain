package de.danisbu.thw.trainingplan.modell

import kotlinx.datetime.LocalDateTime

data class RegistrationDeadline (val value: LocalDateTime)
