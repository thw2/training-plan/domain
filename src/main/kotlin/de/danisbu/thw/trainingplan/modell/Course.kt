package de.danisbu.thw.trainingplan.modell

public data class Course(
    val title: Title,
    val type: Type,
    val id: CourseId,
)