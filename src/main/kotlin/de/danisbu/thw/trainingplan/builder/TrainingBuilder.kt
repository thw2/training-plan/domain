package de.danisbu.thw.trainingplan.builder

import de.danisbu.thw.trainingplan.getCurrentLocalDate
import de.danisbu.thw.trainingplan.modell.*

class TrainingBuilder(
    var id: TrainingId = TrainingId(""),
    var start: Start = Start(getCurrentLocalDate()),
    var end: End = End(getCurrentLocalDate()),
    var registrationDeadline: RegistrationDeadline? = RegistrationDeadline(getCurrentLocalDate()),
    var courseBuilder: CourseBuilder = CourseBuilder(Title(""), Type(""), CourseId("")),
    var location: Location = Location("")
) {
    fun built(): Training {
        return Training(id, start, end, registrationDeadline, courseBuilder.built(), location)
    }
}