package de.danisbu.thw.trainingplan.builder

import de.danisbu.thw.trainingplan.modell.Course
import de.danisbu.thw.trainingplan.modell.CourseId
import de.danisbu.thw.trainingplan.modell.Title
import de.danisbu.thw.trainingplan.modell.Type

class CourseBuilder (var title: Title, var type: Type, var id: CourseId) {
    fun built(): Course {
        return Course(title, type, id)
    }
}