val kotlin_version: String by project

plugins {
    application
    kotlin("jvm") version "1.8.22"
    `maven-publish`
}

repositories {
    mavenLocal()
    maven {
        url = uri("https://repo1.maven.org/maven2/")
    }

    maven {
        url = uri("https://repo.maven.apache.org/maven2/")
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.8.22")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")

    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:1.8.22")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.3")
}

group = "de.danisbu.thw.trainingplan"
version = "1.1-SNAPSHOT"
description = "THW training catalog domain"

publishing {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/40318647/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = findProperty("tokenType") as String?
                value = findProperty("gitlabDeployToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

tasks.withType<JavaCompile>() {
    options.encoding = "UTF-8"
}
